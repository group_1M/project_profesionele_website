<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';
// Instantiate a new instance of the translator for getting the texts displayed on the page.
$translator = new \Project\Translator( DEBUG_MODE );
// Set the translator prefix so you don't have to call the key prefix every time you want to output some text.
$translator->setFileKeyPrefix( 'sitemap' );
?>
<!DOCTYPE html>
<html lang="<?= $translator->getLanguage() ?>">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <title><?= $translator->getText('page_title') ?></title>
    <?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->renderHead() : '' ?>
</head>
<body>

<div class="page-wrapper container-fluid">

    <!-- The top header of the webpage with the logo and navigation of the website -->
    <header class="header-wrapper col-12">

        <!-- The header wrapper that contains the torvalds hogeschool logo -->
        <div class="header-left col-2">
            <a href="index.php">
                <img src="images/logo_text.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2"/></a>
        </div>

        <!-- The header wrapper that contains the sites navigation an language options -->
        <div class="header-right col-10">

            <!-- The wrapper that contains the sites navigation menu and search function -->
            <nav class="nav-wrapper col-12">
                <ul>

                    <li class="">
                        <a href="opleidingen.php"><?= $translator->getText( 'layout.menu_opleidingen' ) ?></a>
                    </li>

                    <li>
                        <a href="opendagen.php"><?= $translator->getText( 'layout.menu_opendagen' ) ?></a>
                    </li>

                    <li>
                        <a href="contact.php"><?= $translator->getText( 'layout.menu_contact' ) ?></a>
                    </li>

                    <li>
                        <a href="overons.php"><?= $translator->getText( 'layout.menu_over_ons' ) ?></a>
                    </li>

                    <li>
                        <label for="search"></label>
                        <input name="search" id="search" type="text"
                               placeholder="<?= $translator->getText( 'layout.menu_zoek' ) ?>"/>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- The wrapper that contains the buttons for switching the sites language -->
        <div class="lang-wrapper col-2 offset-8">

            <a href="?lang=nl">
                <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
            </a>

            <div class="flag-spacer col-1">
                <div class="flag-spacer-item"></div>
            </div>

            <a href="?lang=en">
                <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
            </a>

        </div>

    </header>

    <!-- The full width image on the home page -->
    <div class="header-image-wrapper col-12">
        <img src="images/background.jpg" class="header-background-image" alt="People working on computer">
    </div>

    <main class="main-wrapper offset-1 col-10">

        <section class="col-12">
            <h2 class="color-heading-2">
                <?= $translator->getText('page_heading_main')?>
            </h2>
            <ul class="sitemap-list">
                <li>
                    <a href="opleidingen.php" class="link-markup" >
                        <?= $translator->getText( 'opleidingen.page_title' ) ?>
                    </a>
                </li>
                <li>
                    <ul class="sitemap-inner-list">
                        <li>
                            <?= $translator->getText('heading_informatica') ?>
                        </li>
                        <li>
                        <ul class="sitemap-inner-list">
                            <li>
                                <a href="softwareengineering.php" class="link-markup">
                                    <?= $translator->getText('software_enginering.page_title') ?>
                                </a>
                            </li>
                            <li>
                                <a href="ictbeheerad.php" class="link-markup">
                                    <?= $translator->getText('ictbeheer_ad.page_title') ?>
                                </a>
                            </li>
                            <li>
                                <a href="technischeinformatica.php" class="link-markup">
                                    <?= $translator->getText('technische_informatieca.page_title') ?>
                                </a>
                            </li>
                            <li>
                                <a href="frontenddeveloper.php" class="link-markup">
                                    <?= $translator->getText('front_end_developer.page_title') ?>
                                </a>
                            </li>
                        </ul>
                        </li>
                    </ul>
                </li>

                <li>
                    <ul class="sitemap-inner-list">
                        <li>
                            <?= $translator->getText('heading_infratechniek') ?>
                        </li>
                        <li>
                            <ul class="sitemap-inner-list">
                                <li>
                                    <a href="technicusdata.php" class="link-markup">
                                        <?= $translator->getText('technicus_data.page_title') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="technicuselektrotechniek.php" class="link-markup">
                                        <?= $translator->getText('technicus_electrotechniek.page_title') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="technicusgas.php" class="link-markup">
                                        <?= $translator->getText( 'technicus_gas.page_title' ) ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="technicusspanningdistributie.php" class="link-markup">
                                        <?= $translator->getText('technicus_planning_distibutie.page_title') ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="opendagen.php" class="link-markup">
                        <?= $translator->getText( 'opendagen.page_title' ) ?>
                    </a>
                </li>
                <li>
                    <a href="evenementen.php" class="link-markup">
                        <?= $translator->getText('evenementen.page_title') ?>
                    </a>
                </li>
                <li>
                    <a href="contact.php" class="link-markup">
                        <?= $translator->getText('contact.page_title') ?>
                    </a>
                </li>
                <li>
                    <a href="overons.php" class="link-markup">
                        <?= $translator->getText('over_ons.page_title') ?>
                    </a>
                <li>
                    <a href="colofon.php" class="link-markup">
                        <?= $translator->getText( 'colofon.page_title' ) ?>
                    </a>
                </li>
            </ul>

        </section>

    </main>
</div>
<footer class="footer-wrapper col-12">

    <a href="contact.php" class="footer-links">
        <h3>Contact</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="colofon.php" class="footer-links">
        <h3>Colofon</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="sitemap.php" class="footer-links">
        <h3>Sitemap</h3>
    </a>

</footer>
<?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->render() : '' ?>
</body>
</html>