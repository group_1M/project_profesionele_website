<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';
$translator = new \Project\Translator( DEBUG_MODE );
$translator->setFileKeyPrefix( 'home' );
?>
<!DOCTYPE html>
<html lang="<?= $translator->getLanguage() ?>">
<head>
    <title>Torvalds hogeschool</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->renderHead() : '' ?>
</head>
<body>

<div class="page-wrapper container-fluid">

    <!-- The top header of the webpage with the logo and navigation of the website -->
    <header class="header-wrapper col-12">

        <!-- The header wrapper that contains the torvalds hogeschool logo -->
        <div class="header-left col-2">
            <a href="index.php">
                <img src="images/logo_text.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2"/>
            </a>
        </div>

        <!-- The header wrapper that contains the sites navigation an language options -->
        <div class="header-right col-10">

            <!-- The wrapper that contains the sites navigation menu and search function -->
            <nav class="nav-wrapper col-12">
                <ul>

                    <li>
                        <a href="opleidingen.php"><?= $translator->getText( 'layout.menu_opleidingen' ) ?></a>
                    </li>

                    <li>
                        <a href="opendagen.php"><?= $translator->getText( 'layout.menu_opendagen' ) ?></a>
                    </li>

                    <li>
                        <a href="contact.php"><?= $translator->getText( 'layout.menu_contact' ) ?></a>
                    </li>

                    <li>
                        <a href="overons.php"><?= $translator->getText( 'layout.menu_over_ons' ) ?></a>
                    </li>

                    <li>
                        <label for="search"></label>
                        <input name="search" id="search" type="text"
                               placeholder="<?= $translator->getText( 'layout.menu_zoek' ) ?>"/>
                    </li>
                </ul>
            </nav>

        </div>

        <!-- The wrapper that contains the buttons for switching the sites language -->
        <div class="lang-wrapper col-2 offset-8">

            <a href="?lang=nl">
                <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
            </a>

            <div class="flag-spacer col-1">
                <div class="flag-spacer-item"></div>
            </div>

            <a href="?lang=en">
                <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
            </a>

        </div>

    </header>

    <!-- The full width image on the home page -->
    <div class="header-image-wrapper col-12">
        <img src="images/background.jpg" class="header-background-image" alt="People working on computer">
    </div>

    <main class="main-wrapper col-10 offset-1 index-main">

        <section class="col-6">
            <h2 class="color-heading-2">Informatica</h2>
            <ul>
                <li>
                    <a href="softwareengineering.php" class="opleiding-blue">Software Engineering</a>
                </li>
                <li>
                    <a href="ictbeheerad.php" class="opleiding-purple">ICT Beheer AD</a>
                </li>
                <li>
                    <a href="technischeinformatica.php" class="opleiding-blue">Technische Informatica</a>
                </li>
                <li>
                    <a href="frontenddeveloper.php" class="opleiding-purple">Front End Developer</a>
                </li>
            </ul>
        </section>

        <section class="col-6">
            <h2 class="color-heading-2">Infratechniek</h2>

            <ul>
                <li>
                    <a href="technicusdata.php" class="opleiding-blue">Technicus Data</a>
                </li>
                <li>
                    <a href="technicuselektrotechniek.php" class="opleiding-purple">Technicus Elektrotechniek</a>
                </li >
                <li>
                    <a href="technicusgas.php" class="opleiding-blue">Technicus Gas</a>
                </li>
                <li>
                    <a href="technicusspanningdistributie.php" class="opleiding-purple">Technicus Spanningdistributie</a>
                </li>
            </ul>

        </section>

        <section class="col-6">
            <h2 class="color-heading-2">
                <?= $translator->getText( 'heading_evenementen' ) ?>
            </h2>
            <br/>
            <address>
                <?= $translator->getText( 'text_evenementen_adres' ) ?>
            </address>
            <br/>
            <p class="widget-text">
                <?= $translator->getText( 'text_evenementen' ) ?>
            </p>
            <br/>
            <br/>
            <a href="evenementen.php">
                <h2 class="color-heading-2">
                    <?= $translator->getText( 'heading_evenementen_link' ) ?>
                </h2>
            </a>
        </section>

        <section class="col-6">
            <h2 class="color-heading-2">
                <?= $translator->getText( 'heading_nieuws' ) ?>
            </h2>
            <?= ( new \Project\RssRenderer( $translator ) )->renderArticles( 3 ) ?>
        </section>

        <section class="col-12">
            <h2 class="color-heading-2">
                <?= $translator->getText( 'heading_location' ) ?>
            </h2>
            <?= ( new \Project\MapsRenderer() )->renderMap() ?>
        </section>

    </main>

    <div class="watermark">
    </div>
</div>

<footer class="footer-wrapper col-12">

    <a href="contact.php" class="footer-links">
        <h3>
            <?= $translator->getText( 'layout.footer_contact' ) ?>
        </h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="colofon.php" class="footer-links">
        <h3>
            <?= $translator->getText( 'layout.footer_colofon' ) ?>
        </h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="sitemap.php" class="footer-links">
        <h3>
            <?= $translator->getText( 'layout.footer_sitemap' ) ?>
        </h3>
    </a>
</footer>

<?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->render() : '' ?>
</body>
</html>