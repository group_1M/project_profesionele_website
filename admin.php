<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 05-11-2016 00:45
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */
require __DIR__ . DIRECTORY_SEPARATOR .'bootstrap.php';
?>
<!DOCTYPE html>
<html lang="EN">
<head>
    <meta charset="UTF-8"/>
    <title>Admin</title>
</head>
<body>
<?php
$translationManager = new \Project\TranslationManager();

var_dump( $translationManager->getXMlArray() );
?>
?>
</body>
</html>
