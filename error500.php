<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 29-10-2016 19:18
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html */
session_start();
$pageLang = isset( $_SESSION[ 'lang' ] ) ? $_SESSION[ 'lang' ] : 'NL';
?>
<!DOCTYPE html>
<html lang="<?= $pageLang ?>">
<head>
    <meta charset="UTF-8"/>
    <title>Er ging iets mis...</title>
    <style>
        .error-message-wrapper {
            width: 500px;
            height: 300px;
            border-top-left-radius: 60px;
            border-top-right-radius: 20px;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 60px;
            border: 1px groove #3d46c5;
        }

        body {
            display: flex;
            justify-content: center;
            align-content: center;
        }

        h1 {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="error-message-wrapper">
    <h1><?= ( $pageLang === 'NL' ? 'Oops er ging iets mis...' : 'Oops something went wrong...' ) ?></h1>
    <h3><?= ( $pageLang === 'NL' ? 'Probeer het later opnieuw of contacteer de beheerder: ' : 'Try again later or contact the administrator: ' ) . '<a href="mailto:webadmin@torvalds.com">webadmin@torvalds.com</a>' ?></h3>
</div>

</body>
</html>
