# Project profesionele website
### Torvalds hogeschool
### Groep 1M 

This repository contains the source code of the assignment: *Project profesionele website*. <br />
From project group INF1M from Stenden university of applied sciences.<br />
An live version of this website can be found on the addess: [146.185.141.142/project](http://146.185.141.142/project)<br />

# Authors:
```javascript
var authors = {
    "Stefan de Jonge" : function () {
        console.log( "Functie: Projectleider && Software Engineer" );
    },
    "Randy Dijkshoorn" : function () {
        console.log( "Functie: Software Engineer && Designer" );
    },
    "Laura Rengers" : function () {
        console.log( "Functie: Notulist && Software Engineer && Designer" );
    },
    "Maarten de Lange" : function(){
        console.log( "Functie: Designer && Software Enginee");
    }
    "Eshwin Maluku" : function () {
        console.log( "Functie: Software Enginee && Designer" );
    },
    "Joris Rietveld" function () {
        console.log( "Functie: Lead Software Engeneer && Version Controll Mananger" );
    }
};
```

# The project todo's 
> If you fixed some error please click on the pen sign on the right of this page heading
> for editing this readme page and update the todo list. You can update an done task
> by adding an X inside the block brackets

- [x] text.xml
    - [x] Fix translation keys in wrong namespaces.
- [x] opleidingen.php
    - [x] Make the main div centerd on page.
- [x] index.php
    - [x] Fix bug with english translation on rss feed.
    - [x] Add english translation in the events section.
    - [x] Fix exception thrown on english page: Return value of Project\Translator::makeYandexApiCall() must be of the type array, null returned
- [x] softwareengineering.php
    - [x] Add heading image for this page that is not the same as the index.php
    - [x] Make the main div centerd on page.
- [x] ictbeheerad.php
    - [x] Make the main div centerd on page.
- [x] technischeinformatica.php
    - [x] Make the main div centerd on page.
- [x] frontenddeveloper.php
    - [x] Make the main div centerd on page.
- [x] technicusdata.php
    - [x] Fix exception thrown on dutch page: Illegal string offset 'message' in /var/www/project/src/Project/Validator/W3ValidatorDataCollector.php on line 26
    - [x] Make the main div centerd on page.
- [x] technicuselektrotechniek.php
    - [x] Make the main div centerd on page.
- [x] technicusgas.php
    - [x] Make the main div centerd on page.
- [x] technicusspanningdistributie.php
    - [x] Fix exception thrown on dutch page: The text: text_technicus_spanningdistributie is not found in text.xml
    - [x] Fix exception thrown on enlisch page: The text: text_technicus_spanningdistributie is not found in text.xml
- [x] opendagen.php
    - [x] Fix exception thrown on dutch page: The text: textop is not found in text.xml
    - [x] Fix exception thrown on english page: The text: textop is not found in text.xml
    - [x] *Possible error because of the exception* Fix W3 error: End of file seen and there were open elements. on line: 114
    - [x] *Possible error because of the exception* Fix W3 error: Unclosed element “main”. on line: 89
    - [x] *Possible error because of the exception* Fix W3 error: Unclosed element “div”. on line: 21
- [x] contact.php
    - [x] Add global button style to submit button on enlish and dutch page.
    - [x] Make the main div centerd on page.
- [x] overons.php
    - [x] Make the main div centerd on page.
- [x] colofon.php
    - [x] Fix exeption thrown on dutch page: The text: txtcol is not found in text.xml
    - [x] Fix exeption thrown on english page: The text: txtcol is not found in text.xml
    - [x] *Possible error because of the exception* Fix W3 error: End of file seen and there were open elements.   on line: 114
    - [x] *Possible error because of the exception* Fix W3 error: Unclosed element “section”.   on line: 89
    - [x] *Possible error because of the exception* Fix W3 error: Unclosed element “main”.   on line: 88
    - [x] *Possible error because of the exception* Fix W3 error: Unclosed element “div”.   on line: 22
    - [x] *Possible error because of the exception* Fix W3 error: Section lacks heading. Consider using “h2”-“h6” elements to add identifying headings to all sections.   on line: 89
- [x] sitemap.php
    - [x] Fatal exception that stops rendering of the webpage on all languages.

# The logo:
![alt text](/images/logo_default.png)