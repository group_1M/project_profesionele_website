<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$translator = new \Project\Translator( DEBUG_MODE );
$translator->setFileKeyPrefix( 'contact' );
?>
<!DOCTYPE html>
<html lang="<?= $translator->getLanguage() ?>">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <title>Torvalds hogeschool</title>
    <?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->renderHead() : '' ?>
</head>
<body>

<div class="page-wrapper container-fluid">

    <!-- The top header of the webpage with the logo and navigation of the website -->
    <header class="header-wrapper col-12">

        <!-- The header wrapper that contains the torvalds hogeschool logo -->
        <div class="header-left col-2">
            <a href="index.php">
                <img src="images/logo_text.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2"/>
            </a>
        </div>

        <!-- The header wrapper that contains the sites navigation an language options -->
        <div class="header-right col-10">

            <!-- The wrapper that contains the sites navigation menu and search function -->
            <nav class="nav-wrapper col-12">
                <ul>

                    <li class="">
                        <a href="opleidingen.php"><?= $translator->getText( 'layout.menu_opleidingen' ) ?></a>
                    </li>

                    <li>
                        <a href="opendagen.php"><?= $translator->getText( 'layout.menu_opendagen' ) ?></a>
                    </li>

                    <li>
                        <a href="contact.php"><?= $translator->getText( 'layout.menu_contact' ) ?></a>
                    </li>

                    <li>
                        <a href="overons.php"><?= $translator->getText( 'layout.menu_over_ons' ) ?></a>
                    </li>

                    <li>
                        <label for="search"></label>
                        <input name="search" id="search" type="text"
                               placeholder="<?= $translator->getText( 'layout.menu_zoek' ) ?>"/>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- The wrapper that contains the buttons for switching the sites language -->
        <div class="lang-wrapper col-2 offset-8">

            <a href="?lang=nl">
                <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
            </a>

            <div class="flag-spacer col-1">
                <div class="flag-spacer-item"></div>
            </div>

            <a href="?lang=en">
                <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
            </a>

        </div>

    </header>

    <!-- The full width image on the home page -->
    <div class="header-image-wrapper col-12">
        <img src="images/female-college-student-backpack.jpg" class="header-background-image" alt="rugzak">
    </div>
    <main class="main-wrapper col-12">

        <section class="col-4">
            <h2 class="color-heading-2">Contact</h2>
            <br/>
            <form action="submit.php" method="post" class="form-contact">
                <label for="form-name" class="col-12">Naam:*</label>
                <input type="text" name="form-name" id="form-name" class="col-12" required />

                <label for="form-email" class="col-12">E-mailadres:*</label>
                <input type="text" name="form-email" id="form-email" class="col-12" required />

                <label for="form-subject" class="col-12">Onderwerp:*</label>
                <input type="text" name="form-object" id="form-subject" class="col-12" required />

                <label for="form-message" class="col-12">Bericht:*</label>
                <textarea name="form-message" id="form-message" rows="10" cols="50" class="col-12" required ></textarea>

                <input type="submit" name="submit" value="Verzenden">
            </form>
        </section>
    </main>
</div>

<footer class="footer-wrapper col-12">

    <a href="contact.php" class="footer-links">
        <h3>Contact</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="colofon.php" class="footer-links">
        <h3>Colofon</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="sitemap.php" class="footer-links">
        <h3>Sitemap</h3>
    </a>

</footer>
<?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->render() : '' ?>
</body>
</html>