<?php
/**
 * Start an session and set the language to the default if the language wasn't initiated before.
 */
session_start();

// Initiate the language session for cross webpage language settings.
$_SESSION[ 'lang' ] = $_SESSION[ 'lang' ] ?? 'nl';
// When an user switches language save it to the session so we can use it to get the appropriate text.
$_SESSION[ 'lang' ] = $_GET[ 'lang' ] ?? $_SESSION[ 'lang' ];

define( "DIR_UP", '..' . DIRECTORY_SEPARATOR );
define( "WEBSERVER_ROOT", __DIR__ . DIRECTORY_SEPARATOR );

// Only set to true on development servers.
define( "DEBUG_MODE", true );
// Hack to disable the validator otherwise you will get an endless curl loop.
define( 'ACTIVATE_VALIDATOR', !isset( $_GET[ 'validate' ] ) );

ini_set( 'display_errors', TRUE );
ini_set( 'log_errors', !DEBUG_MODE );

/**
 * Register an autoloader for auto loading classes.
 */
spl_autoload_register( function ( $className )
{
    // Check if this is the correct autoloader for autoloading the class file.
    $defaultNamespace = 'Project';
    if ( 0 === strpos( $defaultNamespace, $className ) )
    {
        // Namespace not from this project so move to the next registered autoloader.
        return;
    }

    // Get the base directory of the project. Something like /var/www/www.project.com/
    $baseDir = __DIR__ . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;

    // Translate the full class name to an file on the file system.
    $classPath = $baseDir . str_replace( '\\', DIRECTORY_SEPARATOR, $className ) . '.php';
    file_exists( $classPath ) ? include $classPath : trigger_error( sprintf( 'File not found: %s', $classPath ) );
});

// Register composer autoloader for development libraries such as phpunit and debugbar.
require WEBSERVER_ROOT . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Set the default exception handlers depending on the current environment.
 */
if ( DEBUG_MODE )
{
    $debugBarHelper = \Project\DebugBarHelper::getInstance();

    set_exception_handler( function ( \Throwable $exception ) use ( $debugBarHelper )
    {
        $debugBarHelper->debugBarInstance[ 'exceptions' ]->addThrowable( $exception );
        echo $debugBarHelper->debugBarRenderer->render();
    } );
}
else
{
    set_exception_handler( function ( \Throwable $exception )
    {
        // Redirect the user to the error 500 page so they are informed that something went wrong.
        header( 'location: error500.php' );
    } );
}