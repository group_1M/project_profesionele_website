<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$translator = new \Project\Translator( DEBUG_MODE );
$translator->setFileKeyPrefix( 'opleidingen' );
?>
<!DOCTYPE html>
<html lang="<?= $translator->getLanguage() ?>">
<head>
	<title><?= $translator->getText('page_title') ?></title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->renderHead() : '' ?>
</head>
<body>

<div class="page-wrapper container-fluid">

    <!-- The top header of the webpage with the logo and navigation of the website -->
    <header class="header-wrapper col-12">

        <!-- The header wrapper that contains the torvalds hogeschool logo -->
        <div class="header-left col-2">
            <a href="index.php">
                <img src="images/logo_text.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2"/>
            </a>
        </div>

        <!-- The header wrapper that contains the sites navigation an language options -->
        <div class="header-right col-10">

            <!-- The wrapper that contains the sites navigation menu and search function -->
            <nav class="nav-wrapper col-12">
                <ul>

                    <li class="">
                        <a href="opleidingen.php"><?= $translator->getText( 'layout.menu_opleidingen' ) ?></a>
                    </li>

                    <li>
                        <a href="opendagen.php"><?= $translator->getText( 'layout.menu_opendagen' ) ?></a>
                    </li>

                    <li>
                        <a href="contact.php"><?= $translator->getText( 'layout.menu_contact' ) ?></a>
                    </li>

                    <li>
                        <a href="overons.php"><?= $translator->getText( 'layout.menu_over_ons' ) ?></a>
                    </li>

                    <li>
                        <label for="search"></label>
                        <input name="search" id="search" type="text"
                               placeholder="<?= $translator->getText( 'layout.menu_zoek' ) ?>"/>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- The wrapper that contains the buttons for switching the sites language -->
        <div class="lang-wrapper col-2 offset-8">

            <a href="?lang=nl">
                <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
            </a>

            <div class="flag-spacer col-1">
                <div class="flag-spacer-item"></div>
            </div>

            <a href="?lang=en">
                <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
            </a>

        </div>

    </header>

    <!-- The full width image on the home page -->
    <div class="header-image-wrapper col-12">
        <img src="images/2864d0b0.jpg" class="header-background-image" alt="teacher">
    </div>

    <main class="main-wrapper col-12">
        <div class="col-6 opleiding-column">
            <h2 class="color-heading-2"><?= $translator->getText( 'header_informatica' ) ?></h2>
            <br/>

            <p> <?= $translator->getText( 'text_neem_kijkje_im' ) ?></p>
            <br/>
            <ul class="punt">
                <li>
                    <a href="softwareengineering.php" class="opleiding-blue">
                        <?= $translator->getText( 'link_software_engineering' ) ?></a>
                </li>
                <li>
                    <a href="ictbeheerad.php" class="opleiding-purple">
                        <?= $translator->getText( 'link_ict_beheer_ad' ) ?></a>
                </li>
                <li>
                    <a href="technischeinformatica.php" class="opleiding-blue">
                        <?= $translator->getText( 'link_technische_informatica' ) ?></a>
                </li>
                <li>
                    <a href="frontenddeveloper.php" class="opleiding-purple">
                        <?= $translator->getText( 'link_front_end_developer' ) ?></a>
                </li>
            </ul>
        </div>

        <div class="col-6 opleiding-column">
            <h2 class="color-heading-2"><?= $translator->getText( 'header_infratechniek' ) ?></h2>
            <br/>

            <p> <?= $translator->getText( 'text_neem_kijkje_wb' ) ?></p>
            <br/>

            <ul class="punt">
                <li>
                    <a href="technicusdata.php" class="opleiding-blue">
                        <?= $translator->getText( 'link_technicus_data' ) ?></a>
                </li>
                <li>
                    <a href="technicuselektrotechniek.php" class="opleiding-purple">
                        <?= $translator->getText( 'link_technicus_elektrotechniek' ) ?></a>
                </li >
                <li>
                    <a href="technicusgas.php" class="opleiding-blue">
                        <?= $translator->getText( 'link_technicus_gas' ) ?></a>
                </li>
                <li>
                    <a href="technicusspanningdistributie.php" class="opleiding-purple">
                        <?= $translator->getText( 'link_technicus_spanningdistributie' ) ?></a>
                </li>
            </ul>
        </div>

    </main>


    <div class="watermark">
    </div>

</div>

<footer class="footer-wrapper col-12">

    <a href="contact.php" class="footer-links">
        <h3>Contact</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="colofon.php" class="footer-links">
        <h3>Colofon</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="sitemap.php" class="footer-links">
        <h3>Sitemap</h3>
    </a>

</footer>
<?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->render() : '' ?>
</body>
</html>