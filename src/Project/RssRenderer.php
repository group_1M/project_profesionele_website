<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 03-11-2016 16:41
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project;


class RssRenderer
{
    protected $rssParser;
    protected $translator;
    protected $translateLinks = TRUE;

    public function __construct( Translator $translator = null )
    {
        $this->translator = $translator ? $translator : new Translator( DEBUG_MODE );
        $this->rssParser = new RssAndAtom\RssParser();
    }

    public function renderArticles( int $amount = 3, $truncateText = 160 )
    {
        try
        {
            ob_start();

            echo '<dl class="news-feed-list">';

            foreach ($this->getArticles( $amount ) as $article)
            {
                echo '<dt>';
                echo '<a href="' . $article->getLink() . '">' . $article->getTitle() . '</a>';
                echo '</dt>';
                echo '<dd style="font-size: 14px;">' . substr( $article->getDescription(), 0, $truncateText ) . '...<a class="rss-read-more" href="' . $article->getLink() . '">[' . $this->translator->getText( 'link_rss_lees_verder' ) . ']</a></dd>';

            }
            echo '</dl>';
            $renderedOutput = ob_get_clean();
        }
        catch ( \Exception $exception )
        {
            DebugBarHelper::addDebugException( $exception );
            $renderedOutput = $this->translator->getText( 'error.webservice_tweakers' );
        }
        return $renderedOutput;
    }

    protected function translateArticles( array $newsArticles )
    {
        $translateItems = [];

        foreach ( $newsArticles as $article )
        {
            $translateItems[] = $article->getTitle();
            $translateItems[] = $article->getDescription();
        }

        $result = $this->translator->translate( $translateItems, 'nl', $this->translator->getLanguage() );

        if( isset( $result[ 'text' ] ))
        {
            $result = $result['text'];
            $offset = 0;

            for ($counter = 0; $counter < ( count( $newsArticles )-1) ; $counter++)
            {
                $newsArticles[ $counter ]->setTitle( $result[ $offset ] );
                $offset++;
                $newsArticles[ $counter ]->setDescription( $result[ $offset ] );
                $offset++;

                if( $this->translateLinks)
                {
                    $newsArticles[ $counter ]->setLink( 'https://translate.google.com/translate?hl=en&sl=auto&tl=en&u=' . urlencode( $newsArticles[ $counter ]->getLink() ) );
                }
            }
        }
        return $newsArticles;
    }

    protected function getArticles( int $amount )
    {
        $feed = $this->rssParser->getFeed( 'http://feeds2.feedburner.com/tweakers/mixed' );
        $newsArticles = $feed->getNewsElements( $amount );

        if( $this->translator->getLanguage() === 'en' )
        {
            $newsArticles = $this->translateArticles( $newsArticles );
        }

        return $newsArticles;
    }
}