<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 29-10-2016 21:51
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project\Validator;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;

class W3ValidatorDataCollector extends DataCollector implements Renderable
{
    /**
     * Collect W3 validation data so it can be rendered in the debug bar.
     *
     * @return array
     */
    public function collect() : array
    {
        $messages = $this->callW3Api()[ 'messages' ];
        $counter = 0;
        foreach ($messages as $message)
        {
            $messages[ $counter ] = '<i style="color: red; margin-right: 10px;" class="phpdebugbar-fa phpdebugbar-fa-exclamation-triangle"></i>' . trim( $message[ 'message' ] . '&nbsp;&nbsp; on line: ' . $message[ 'lastLine' ] );
            $counter++;
        }

        if ( count( $messages ) == 0 )
        {
            $messages = [ '<i style="color: green; margin-right: 10px;" class="phpdebugbar-fa phpdebugbar-fa-check-circle"></i> Valid HTML 5 document.' ];
        }
        return [
            'messages' => $messages,
            'count' => count( $messages ),
        ];
    }

    /**
     * Call the W3 validator API on the current url.
     *
     * @return array
     */
    protected function callW3Api() : array
    {
        $handle = curl_init();

        curl_setopt_array( $handle, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => 'https://validator.w3.org/nu/?doc=http%3A%2F%2F146.185.141.142' . urlencode( $_SERVER[ 'DOCUMENT_URI' ] . '?validate=true' ) . '&out=json',
            CURLOPT_TIMEOUT_MS => 1000,
            CURLOPT_HTTPHEADER => [
                'User-Agent: W3 Validation bot for school project.'
            ]
        ] );

        $result = json_decode( curl_exec( $handle ), true );

        return $result ? $result : [
            'messages' => [
                'message' => 'Error communication with W3 validator.',
                'lastLine' => 0
            ]
        ];
    }

    /**
     * Configure the the correct widget for this data collector.
     * @return array
     */
    public function getWidgets()
    {
        return [
            $this->getName() => [
                'icon' => 'html5',
                'tooltip' => 'W3 validator',
                'widget' => 'PhpDebugBar.Widgets.ListWidget',
                'map' => $this->getName() . '.messages',
                'default' => '[]'
            ],
            $this->getName() . ':badge' => [
                'map' => $this->getName() . '.count',
                'default' => 'null'
            ]
        ];
    }

    /**
     * Return the name of the data collector.
     *
     * @return string
     */
    public function getName() : string
    {
        return 'W3Validator';
    }
}