<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 29-10-2016 16:05
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project;


use phpDocumentor\Reflection\Project;
use Project\Exceptions\CurlException;

class Translator
{
    /**
     * An \SimpleXMLElement that contains the text.xml file for loading text on the page.
     * @var \SimpleXMLElement
     */
    protected $text;

    /**
     * An prefix that can be set so you don't have to type the page namespace every time you want to load text.
     * @var string
     */
    protected $fileKeyPrefix;

    /**
     * The current language the translator will search for in the text.xml file.
     * @var string
     */
    protected $language;

    /**
     * Set this to true if you want to throw exceptions when an key can not be found in the text.xml file.
     * @var bool
     */
    protected $throwExceptionMissingText;

    /**
     * The yandex translation api key for authenticating an api call.
     * @var string
     */
    protected $yandexApiKey = 'trnsl.1.1.20161008T210816Z.46085af52648e1ee.b89bab2fe163a83fbe6413d38322a11818d9630b';

    /**
     * The base url for the Yandex translation api.
     * @var string
     */
    protected $yandexApiAddress = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    /**
     * The languages to translate {$fromLanguage}-{$toLanguage}
     * @var string
     */
    protected $yandexTranslateLanguage = 'nl-en';

    /**
     * Translator constructor.
     * @param bool $throwExceptionMissingText
     */
    public function __construct( bool $throwExceptionMissingText = false )
    {
        $this->throwExceptionMissingText = $throwExceptionMissingText;
        $this->loadText();
        $this->setFileKeyPrefix( '' );
        $this->setLanguage( isset( $_SESSION[ 'lang' ] ) ? $_SESSION[ 'lang' ] : 'nl' );
    }

    /**
     * Load the text.xml file as an \SimpleXMLElement so we can use it to search for text.
     *
     * @throws \Exception
     */
    public function loadText()
    {
        $textFile = WEBSERVER_ROOT . 'config' . DIRECTORY_SEPARATOR . 'text.xml';
        if ( file_exists( $textFile ) )
        {
            $this->text = new \SimpleXMLElement( file_get_contents( $textFile ) );
            return;
        }
        throw new \Exception( 'The configuration file: ' . $textFile . ' can\'t be found' );
    }

    /**
     * Get text from text.xml based on key using dot notation.
     *
     * @param string $key
     * @param string $language
     * @return string
     * @throws \Exception
     */
    public function getText( string $key, string $language = '' ) : string
    {
        $this->language = strlen( $language ) > 0 ? mb_strtolower( $language ) : $this->language;

        $textArray = $this->searchForText( $key );

        if ( array_key_exists( $this->language, $textArray ) )
        {
            return (string)$textArray[ $this->language ];
        }

        $message = 'The language: ' . $this->language . ' is not defined for the key: ' . $key;

        if ( $this->throwExceptionMissingText )
        {
            throw new \Exception( $message );
        }
        else
        {
            DebugBarHelper::getInstance()->debugBarInstance[ 'messages' ]->warning( $message );
        }

        return '';
    }

    /**
     * Search the text.xml file for the given key and or key prefix and return the xml nodes as an array.
     *
     * @param string $key
     * @return array
     */
    protected function searchForText( string $key ) : array
    {
        $key = explode( '.', trim( $key ) );

        if ( isset( $this->text->{isset( $key[ 0 ] ) ? $key[ 0 ] : ''} ) && isset( $this->text->{$key[ 0 ]}->{isset( $key[ 1 ] ) ? $key[ 1 ] : ''} ) )
        {
            return (array)$this->text->{$key[ 0 ]}->{$key[ 1 ]};
        }
        elseif ( isset( $this->text->{$this->getFileKeyPrefix()} ) && isset( $this->text->{$this->getFileKeyPrefix()}->{isset( $key[ 0 ] ) ? $key[ 0 ] : ''} ) )
        {
            return (array)$this->text->{$this->fileKeyPrefix}->{$key[ 0 ]};
        }

        $message = 'The text: ' . implode( '.', $key ) . ' is not found in text.xml';
        if ( $this->throwExceptionMissingText )
        {
            throw new \LogicException( $message );
        }
        else
        {
            DebugBarHelper::getInstance()->debugBarInstance[ 'messages' ]->warning( $message );
        }

        return [];
    }

    /**
     * @return mixed
     */
    public function getFileKeyPrefix() : string
    {
        return $this->fileKeyPrefix;
    }

    /**
     * @param mixed $fileKeyPrefix
     * @return Translator
     */
    public function setFileKeyPrefix( string $fileKeyPrefix ) : Translator
    {
        $this->fileKeyPrefix = $fileKeyPrefix;
        return $this;
    }

    /**
     * Translate text using the Yandex translation api.
     *
     * @param string $sentence
     * @return string
     */
    public function translate( array $texts, string $fromLang = 'nl', string $toLang = 'en' ):array
    {
        $this->setYandexTranslateLanguage( $fromLang, $toLang );

        return $this->makeYandexApiCall( $texts );
    }

    /**
     * Make an call to the api to translate the text.
     *
     * @param string $sentence
     * @return mixed
     */
    protected function makeYandexApiCall( array $texts ) : array
    {
        $handle = curl_init( $this->generateUrl( $texts ) );

        curl_setopt_array( $handle, [
            CURLOPT_RETURNTRANSFER=> TRUE,
            CURLOPT_TIMEOUT_MS => 15000,
            CURLOPT_VERBOSE => TRUE,
        ]);

        $curlResult = curl_exec( $handle );

        if( $curlResult === false && $this->isThrowExceptionMissingText() )
        {
            throw new CurlException( curl_error( $handle ) );
        }
        elseif( $curlResult === false && !( $this->isThrowExceptionMissingText() ) )
        {
            DebugBarHelper::addDebugException( new CurlException( curl_error( $handle ) ) );
            $result = [];
        }
        else
        {
            $result = json_decode( $curlResult, TRUE );

            if( count( $curlResult) < 1 )
            {
                DebugBarHelper::addDebugMessage( 'Json decode error data: ' );
            }
        }

        curl_close( $handle );

        return $result;
    }

    /**
     * Generate the Yandex translation url.
     *
     * @param string $translateSentence
     * @return string
     */
    protected function generateUrl( array $texts ) : string
    {
        $urlText ='';

        foreach ( $texts as $text )
        {
            $urlText .= '&text=' . $text;
        }

        return $this->yandexApiAddress . '?' .
        'key=' . $this->yandexApiKey .
        $urlText .
        '&lang=' . $this->yandexTranslateLanguage;
    }

    /**
     * @return mixed
     */
    public function getLanguage() : string
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     * @return Translator
     */
    public function setLanguage( string $language ) : Translator
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isThrowExceptionMissingText(): bool
    {
        return $this->throwExceptionMissingText;
    }

    /**
     * @param boolean $throwExceptionMissingText
     * @return Translator
     */
    public function setThrowExceptionMissingText( bool $throwExceptionMissingText ): Translator
    {
        $this->throwExceptionMissingText = $throwExceptionMissingText;
        return $this;
    }

    /**
     * @return string
     */
    public function getYandexApiKey(): string
    {
        return $this->yandexApiKey;
    }

    /**
     * @param string $yandexApiKey
     * @return Translator
     */
    public function setYandexApiKey( string $yandexApiKey ): Translator
    {
        $this->yandexApiKey = $yandexApiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getYandexApiAddress(): string
    {
        return $this->yandexApiAddress;
    }

    /**
     * @param string $yandexApiAddress
     * @return Translator
     */
    public function setYandexApiAddress( string $yandexApiAddress ): Translator
    {
        $this->yandexApiAddress = $yandexApiAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getYandexTranslateLanguage(): string
    {
        return $this->yandexTranslateLanguage;
    }

    /**
     * @param string $yandexTranslateLanguage
     * @return Translator
     */
    public function setYandexTranslateLanguage( string $fromLanguage, string $toLanguage ): Translator
    {
        $this->yandexTranslateLanguage = $fromLanguage . '-' . $toLanguage;
        return $this;
    }
}
