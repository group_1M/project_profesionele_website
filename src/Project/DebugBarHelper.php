<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 29-10-2016 19:26
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project;


use DebugBar\StandardDebugBar;
use Project\Validator\W3ValidatorDataCollector;

class DebugBarHelper
{
    /**
     * Static debug bar.
     * @var \Project\DebugBarHelper
     */
    protected static $debugBarHelperInstance;

    /**
     * The debug bar instance with data collectors.
     * @var StandardDebugBar
     */
    public $debugBarInstance;

    /**
     * The debug bar renderer for displaying the debug bar.
     * @var \DebugBar\JavascriptRenderer
     */
    public $debugBarRenderer;

    /**
     * The constructor is protected so this class can only get initiated once.
     * DebugBarHelper constructor.
     */
    protected final function __construct()
    {
        $this->debugBarInstance = new StandardDebugBar();
        if ( ACTIVATE_VALIDATOR )
        {
            $this->debugBarInstance->addCollector( new W3ValidatorDataCollector() );
        }
        $this->debugBarRenderer = $this->debugBarInstance->getJavascriptRenderer();
    }

    /**
     * Singleton for access to the StandardDebugBar.
     * @return DebugBarHelper
     */
    public static function getInstance() : DebugBarHelper
    {
        if ( self::$debugBarHelperInstance === null )
        {
            self::$debugBarHelperInstance = new self();
        }
        return self::$debugBarHelperInstance;
    }

    /**
     * Helper for quickly adding debug messages.
     * @param string $message
     * @param array $context
     */
    public static function addDebugMessage( string $message, array $context =[] )
    {
        self::getInstance()->debugBarInstance['messages']->debug( $message, $context );
    }

    /**
     * Helper for quickly setting an debug exception.
     * @param \Throwable $exception
     */
    public static function addDebugException( \Throwable $exception )
    {
        self::getInstance()->debugBarInstance['exceptions']->addThrowable( $exception );
    }
}