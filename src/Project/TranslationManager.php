<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 05-11-2016 00:44
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project;


class TranslationManager
{
    protected $xmlObject;
    protected $xmlArray;
    protected $xmlFile = WEBSERVER_ROOT . 'config' . DIRECTORY_SEPARATOR . 'text.xml';

    protected $namespaces;

    public function __construct(  )
    {
        $this->xmlObject = simplexml_load_file( $this->xmlFile );
    }

    protected function getNamespaces(  )
    {


    }

    public function addNamespace( string $namespaceName )
    {
        
    }

    public function addTranslationKey( string $translationKey )
    {

    }

    public function addTranslation( string $translationKey, string $translationLanguage, string $translation = '' )
    {

    }

    public function editNamespace( string $namespaceName, string $newName )
    {
        
    }

    public function editTranslationKey( string $translationKey, string $newName )
    {

    }

    public function editTranslation( string $translationKey, string $translationLanguage, string $translation )
    {
        
    }

    public function getDomDocument(  )
    {
        return $this->xmlObject;
    }

    public function getXMlArray(  )
    {
        $returnArray = [];

        $rootNode = get_object_vars ( $this->getDomDocument() );

        if( isset( $array['comment'] ))
        {
            unset( $array['comment']);
        }

        $returnArray = $rootNode;

        foreach ( $rootNode as $namespaceName => $namespaceValue )
        {
            $returnArray[ $namespaceName ] = (array)$namespaceValue;

            foreach ( $returnArray[ $namespaceName ] as $key => $value  )
            {
                $returnArray[ $namespaceName ][$key] = (array)$value;
                foreach ( $returnArray[ $namespaceName ][ $key ] as $translationKey => $translationValue )
                {
                    $returnArray[ $namespaceName ][ $key ][ $translationKey ] = (string)$translationValue;
                }
            }

        }

        return $returnArray;
    }
}