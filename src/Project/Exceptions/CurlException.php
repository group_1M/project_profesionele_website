<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 31-10-2016 17:03
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project\Exceptions;


class CurlException extends \Exception
{
    public function __construct( string $message = '', int $code = 0, \Exception $previous = null )
    {
        parent::__construct( $message, $code, $previous );
    }
}