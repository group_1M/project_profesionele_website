<?php
/**
 * Author: Joris Rietveld <jorisrietveld@gmail.com>
 * Created: 03-11-2016 22:15
 * Licence: GNU General Public licence version 3 <https://www.gnu.org/licenses/quick-guide-gplv3.html>
 */

namespace Project;


class MapsRenderer
{
    protected $easyGoogleMapsApi;
    protected $apiKey = 'AIzaSyAHL9jZdGrTkWXe1np536RX4AgTMuaxYrE';
    protected $mapsAddress = 'Van Schaikweg 94, Emmen';
    protected $mapsText = 'Torvalds Hogeschool';

    public function __construct( string $googleMapsApiKey = '' )
    {
        $this->setApiKey( ( strlen( $googleMapsApiKey ) > 0 ) ? $googleMapsApiKey : $this->getApiKey() );
        $this->easyGoogleMapsApi = new EasyGoogleMap( $this->getApiKey() );
    }

    public function renderMap()
    {
        try
        {
            $this->easyGoogleMapsApi->SetAddress( $this->getMapsAddress() );
            $this->easyGoogleMapsApi->SetInfoWindowText( $this->getMapsText() );
            $this->easyGoogleMapsApi->mMapType = TRUE;
            $this->easyGoogleMapsApi->SetMapHeight('400');

            return $this->easyGoogleMapsApi->GmapsKey() .
                $this->easyGoogleMapsApi->MapHolder() .
                $this->easyGoogleMapsApi->InitJs() .
                $this->easyGoogleMapsApi->UnloadMap();
        }
        catch (\Exception $exception )
        {
            if(DEBUG_MODE)
            {
                \Project\DebugBarHelper::addDebugException($exception);
            }
            else {
                return 'Map kan niet geladen worden';
            }
        }
    }

    /**
     * @return EasyGoogleMap
     */
    public function getEasyGoogleMapsApi(): EasyGoogleMap
    {
        return $this->easyGoogleMapsApi;
    }

    /**
     * @param EasyGoogleMap $easyGoogleMapsApi
     * @return MapsRenderer
     */
    public function setEasyGoogleMapsApi( EasyGoogleMap $easyGoogleMapsApi ): MapsRenderer
    {
        $this->easyGoogleMapsApi = $easyGoogleMapsApi;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return MapsRenderer
     */
    public function setApiKey( string $apiKey ): MapsRenderer
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getMapsAddress(): string
    {
        return $this->mapsAddress;
    }

    /**
     * @param string $mapsAddress
     * @return MapsRenderer
     */
    public function setMapsAddress( string $mapsAddress ): MapsRenderer
    {
        $this->mapsAddress = $mapsAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getMapsText(): string
    {
        return $this->mapsText;
    }

    /**
     * @param string $mapsText
     * @return MapsRenderer
     */
    public function setMapsText( string $mapsText ): MapsRenderer
    {
        $this->mapsText = $mapsText;
        return $this;
    }


}