<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$translator = new \Project\Translator( DEBUG_MODE );
$translator->setFileKeyPrefix( 'opleidingen' );
?>
<!DOCTYPE html>
<html lang="<?= $translator->getLanguage() ?>">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <title>Torvalds hogeschool</title>
    <?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->renderHead() : '' ?>
</head>
<body>

<div class="page-wrapper container-fluid">

    <!-- The top header of the webpage with the logo and navigation of the website -->
    <header class="header-wrapper col-12">

        <!-- The header wrapper that contains the torvalds hogeschool logo -->
        <div class="header-left col-2">
            <a href="index.php">
                <img src="images/logo_text.png" alt="Torvalds hogeschool logo" class="brand-logo offset-2"/>
            </a>
        </div>

        <!-- The header wrapper that contains the sites navigation an language options -->
        <div class="header-right col-10">

            <!-- The wrapper that contains the sites navigation menu and search function -->
            <nav class="nav-wrapper col-12">
                <ul>

                    <li class="">
                        <a href="opleidingen.php"><?= $translator->getText( 'layout.menu_opleidingen' ) ?></a>
                    </li>

                    <li>
                        <a href="opendagen.php"><?= $translator->getText( 'layout.menu_opendagen' ) ?></a>
                    </li>

                    <li>
                        <a href="contact.php"><?= $translator->getText( 'layout.menu_contact' ) ?></a>
                    </li>

                    <li>
                        <a href="overons.php"><?= $translator->getText( 'layout.menu_over_ons' ) ?></a>
                    </li>

                    <li>
                        <label for="search"></label>
                        <input name="search" id="search" type="text"
                               placeholder="<?= $translator->getText( 'layout.menu_zoek' ) ?>"/>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- The wrapper that contains the buttons for switching the sites language -->
        <div class="lang-wrapper col-2 offset-8">

            <a href="?lang=nl">
                <img src="images/Netherlands-Flag-icon.png" alt="Dutch flag" class="flag-icon col-5"/>
            </a>

            <div class="flag-spacer col-1">
                <div class="flag-spacer-item"></div>
            </div>

            <a href="?lang=en">
                <img src="images/United-Kingdom-flag-icon.png" alt="English flag" class="flag-icon col-5"/>
            </a>

        </div>

    </header>

    <!-- The full width image on the home page -->
    <div class="header-image-wrapper col-12">
        <img src="images/o-STUDENT-facebook.jpg" class="header-background-image" alt="studentje">
    </div>


    <main class="main-wrapper col-12">
        <section class="col-6">
            <h2 class="color-heading-2">Informatie avonden</h2>
			<br/>
				<p>
				Wil je graag naast je werk een opleiding volgen? 
				Of op zoek naar opleidingsmogelijkheden voor je werknemers? 
				Torvalds heeft een groot aanbod aan opleidingen die je duaal of deeltijd kunt volgen. 
				Zo combineer je werk met het behalen van een Bachelor Degree, Associate Degree of Master Degree.
				Daarnaast zijn sommige opleidingen verkort te volgen. Je haalt dan in één tot drie jaar een hbo-diploma. 
				Wil je meer weten? Kom dan langs tijdens een van de informatie avonden!
				<br/>
				<br/>
				De avonden beginnen om 19:00 en duren tot 21:00.
				Er zijn presentaties over alle verschillende opleidingen.
				Ook is er een informatiemarkt en is er een moment om vragen te stellen aan verschillende docenten.
				<br/>
				<br/>
				<b>03.11.2016, 16.02.2017 & 30.06.2017</b>
				</p>
				
				<br/>
				<br/>
			
        </section>
    </main>
</div>

<footer class="footer-wrapper col-12">

    <a href="contact.php" class="footer-links">
        <h3>Contact</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="colofon.php" class="footer-links">
        <h3>Colofon</h3>
    </a>

    <h3 class="footer-links">|</h3>

    <a href="sitemap.php" class="footer-links">
        <h3>Sitemap</h3>
    </a>

</footer>
<?= DEBUG_MODE ? $debugBarHelper->debugBarRenderer->render() : '' ?>
</body>
</html>